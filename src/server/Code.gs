/**
 * Poll maker - make a poll using Google Forms from a text in Google Docs
 * 
 * @copyright Google Inc., September 28th 2015
 * @author Javier Cañadillas (javiercm@google.com)
 * @version 1.0
 * @tutorial <Insert tutorial URL here>
 */

/**
 * Creates a menu entry in the Google Docs UI when the document is opened.
 *
 * @param {object} e The event parameter for a simple onOpen trigger. To
 *     determine which authorization mode (ScriptApp.AuthMode) the trigger is
 *     running in, inspect e.authMode.
 */
function onOpen(e) {
  DocumentApp.getUi().createAddonMenu()
      .addItem('Process Slogans', 'processSlogans')
      .addItem('Configure Settings', 'configureSettings')
      .addToUi();
}

/**
 * Runs when the add-on is installed.
 *
 * @param {object} e The event parameter for a simple onInstall trigger. To
 *     determine which authorization mode (ScriptApp.AuthMode) the trigger is
 *     running in, inspect e.authMode. (In practice, onInstall triggers always
 *     run in AuthMode.FULL, but onOpen triggers may be AuthMode.LIMITED or
 *     AuthMode.NONE.)
 */
function onInstall(e) {
  onOpen(e);
  showAlertInstall();
}

/**
 * Opens a sidebar in the document containing the add-on's main interface.
 */
function processSlogans() {
  var ui = HtmlService.createTemplateFromFile('Sidebar').evaluate()
    .setSandboxMode(HtmlService.SandboxMode.IFRAME)
    .setTitle('Process Slogans');
  DocumentApp.getUi().showSidebar(ui);
}

/**
 * Opens a sidebar in the document containing the add-on's settings interface.
 */
function configureSettings() {
  var ui = HtmlService.createTemplateFromFile('Settings').evaluate()
    .setSandboxMode(HtmlService.SandboxMode.IFRAME)
    .setTitle('Configure Settings');
  DocumentApp.getUi().showSidebar(ui);
}

/**
 * Gets saved preferences from Script Properties
 *
 * @returns {Object} pollPreferences The object storing preferences
 */
function getPreferences() {
  var pollPreferences = PropertiesService.getDocumentProperties().getProperties();
  return pollPreferences;
}

/**
 * Stores configuration object as Document Properties attributes
 *
 * @param {Object} pollPreferences The preferences object
 * @returns {String} information message
 */
function savePreferences(pollPreferences) {
  // Test if required settins are set
  testPreferenceDeps("To:",pollPreferences.selectMail,pollPreferences.mailTo);
  testPreferenceDeps("Form ID:",pollPreferences.selectForm,pollPreferences.formID);
  try {
    PropertiesService.getDocumentProperties().setProperties(pollPreferences, true);
    return "Settings saved";
  } catch(e) {
    Logger.log("Error %s saving properties", e);
    return "Error saving settings";
  }
}

/**
 * Tests dependencies between a parent checkbox preference and an additional info preference
 *
 * @param {String} name: Required (additional) configuration field name.
 * @param {String} checked: Check-style configuration setting that enables
 *     the appearance of an additional (and required) sub-configuration setting.
 *     For example, if an existing Form is going to be used (selectForm is checked),
 *     then it's required to fill in an additional configuration setting called Form ID.
 * @param {String} additional: The additional (and required) configuration setting
 *     to be filled.
 *
 * @returns {Object} The object storing preferences
 */
function testPreferenceDeps(name, checked, additional) {
  if (checked) {
    if (!additional) {
      var errorMsg = "Field " + name + " cannot be empty";
      throw errorMsg;
    }
  }
}

/**
 * Gets slogans from current document in a different fashion depending on
 * the kind of document (enabled via a document property). If it's a Google
 * standard immersive agenda, a function will be called to extract text
 * from a table that's expected to be there. If not, another function that
 * extracts selected text in the document will be called.
 */
function getSlogans() {
  // Based on selectImm property, get them from table or from selected text.
  var selectImm = PropertiesService.getDocumentProperties().getProperty('selectImm');
  if (selectImm === 'true') {
    return getTextfromImmTable();
  } else {
    return getSelectedText();
  }

}

/**
 * Gets the text the user has selected. If there is no selection,
 * this function displays an error message.
 *
 * @returns {Array} The selected text.
 */
function getSelectedText() {
  var selection = DocumentApp.getActiveDocument().getSelection();
  if (selection) {
    var text = [];
    var elements = selection.getSelectedElements();
    for (var i = 0; i < elements.length; i++) {
      if (elements[i].isPartial()) {
        var element = elements[i].getElement().asText();
        var startIndex = elements[i].getStartOffset();
        var endIndex = elements[i].getEndOffsetInclusive();

        text.push(element.getText().substring(startIndex, endIndex + 1));
      } else {
        var element = elements[i].getElement();
        // Only get elements that can be edited as text; skip images and
        // other non-text elements.
        if (element.editAsText) {
          var elementText = element.asText().getText();
          // This check is necessary to exclude images, which return a blank
          // text element.
          if (elementText != '') {
            text.push(elementText);
          }
        }
      }
    }
    if (text.length == 0) {
      throw 'Please select some text.';
    }
    return text;
  } else {
    throw 'Please select some text.';
  }
}

/**
 * Extracts text from the first table in the document, ignoring the first line
 * present in each cell.
 * 
 * @returns {Array} The text contained in the table cells, line breaks
 *     considered.
 */
function getTextfromImmTable() {
  var docTables = DocumentApp.getActiveDocument().getBody().getTables();
  var numTables = docTables.length;
  var firstTable = docTables[0];
  var numRows = firstTable.getNumRows();
  var fullTextArray = [];
  for (var i=0; i < numRows; i++) {
    var currentRow = firstTable.getRow(i);
    var rowNumCells = currentRow.getNumCells();
    for (var j=0; j < rowNumCells; j++) {
      var currentCell= currentRow.getCell(j);
      var currentCellText = currentCell.getText();
      var currentCellTextArray = currentCellText.match(/[^\r\n]+/g);
      currentCellTextArray.shift();
      var trimmedCellTextArray = currentCellTextArray.map(function(s) { return s.trim() });
      fullTextArray = fullTextArray.concat(trimmedCellTextArray);
    }
  }
  var fullText = fullTextArray.join("\n");
  return fullText;
}

/**
 * Creates a new list item in either a new or preexisting Form containing
 * the list of slogans extracted fromm the document. The choice of a new or
 * existing form is taken based on the pollPreferences.selectForm property
 * value. It will also create and display a QR, and send a mail notification
 * with a link to the form based on the value of pollPreferences.selectQR and
 * pollPreferences.selectMail as preferences markers for them.
 * 
 * @param {Array} slogansText Array containing candidate slogans
 *
 * @returns {String} Returns either nothing or an error message after
 *     failing to perform any of the form opening | creation | mail sending.
 */
function createPoll(slogansText) {
  var pollPreferences = PropertiesService.getDocumentProperties().getProperties();
  var slogansArray = slogansText.match(/[^\r\n]+/g);
  var votingForm;
  if (pollPreferences.selectForm === "true") {
    try {
      votingForm = FormApp.openById(pollPreferences.formID);
    } catch(e) {
      return e;
    }
  } else {
    try {
      // @todo Enable text localisation
      votingForm = FormApp.create("Vote our new campaign slogan!");
    } catch(e) {
      return e;
    }
  };
  votingForm.setRequireLogin(false);
  var slogansList = votingForm.addListItem();
  // @todo Enable text localisation
  slogansList.setTitle("Choose your pick").setChoiceValues(slogansArray);
  var formURL = votingForm.getPublishedUrl();
  
  // Test for QR selection
  if (pollPreferences.selectQR === "true") {
    var qrImageURL = make_QR(formURL);
    displayImage(qrImageURL);
  }
  
  // Test for mail notification preferences
  if (pollPreferences.selectMail === "true") {
    var recipients = pollPreferences.mailTo;
    // @todo Enable text and mail body localisation
    var subject = "Deciding on a campaign slogan";
    var htmlBody = HtmlService.createHtmlOutputFromFile('Mailbody').getContent().replace("%%formURL%%",formURL);
    try {
      GmailApp.sendEmail(recipients, subject,'',{htmlBody:htmlBody});
    } catch(e) {
      return e;
    }
  }  
}

/**
 * Displays a image in an HTML dialog given its URL
 *
 * @param {String} imageURL The image URL to be displayed.
 */
function displayImage(imageURL) {
  var t = HtmlService.createTemplateFromFile('QRUI');
  t.imageURL = imageURL;
  var html = t.evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME)
              .setWidth(410)
              .setHeight(450);
  DocumentApp.getUi()
      .showModalDialog(html, 'Scan the QR code to access the poll');
}

/**
 * Generates a QR code for a given URL
 *
 * @param{String} url URL to generate the image from.
 *
 * @returns{String} image_url URL to the QR image hosted in Google Charts
 */
function make_QR(url) {
  var size = 400;
  var encoded_url = encodeURIComponent(url);  
  var image_url = "http://chart.googleapis.com/chart?chs=" + size + "x" + size + "&cht=qr&chl=" + encoded_url;
  return image_url;
}

/**
 * Shows an HTML dialog warning about add-on installation.
 */
function showAlertInstall() {
  var ui = DocumentApp.getUi();

  var result = ui.alert(
     'You\'re installing the add-on for the first time.',
     'Do you want to configure its settings now?',
      ui.ButtonSet.YES_NO);

  // Process the user's response.
  if (result == ui.Button.YES) {
    // User clicked "Yes".
    configureSettings();
  }
}

/**
 * Displays an HTML-service dialog in Google Sheets that contains client-side
 * JavaScript code for the Google Picker API.
 */
function showPicker() {
  var html = HtmlService.createHtmlOutputFromFile('Picker.html')
      .setWidth(600)
      .setHeight(425)
      .setSandboxMode(HtmlService.SandboxMode.IFRAME);
  DocumentApp.getUi().showModalDialog(html, 'Select a file');
}

// Helper function that puts external JS / CSS into the HTML file.
function include(filename) {
  return HtmlService.createHtmlOutputFromFile(filename)
      .getContent();
}